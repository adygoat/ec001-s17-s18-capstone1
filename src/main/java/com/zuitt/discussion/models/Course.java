package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue
    public Long id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double price;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    //@JoinColumn(name="user_id", nullable = false)
    private User user;

    public Course() {
    }

    public Course( String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }


    public void setUser(User user) {

        this.user = user;
    }

    public User getUser() {

        return user;
    }

    public Long getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {

        return price;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public void setPrice(Double price) {

        this.price = price;
    }
}
